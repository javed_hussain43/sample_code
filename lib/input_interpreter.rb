require_relative "./parking_constant"

class InputInterpreter
  include ParkingConstant

  attr_reader :query
  
  def initialize(query:)
    @query = query.split(" ")
  end

  def call
    guess_operation
  end


  private

  def guess_operation
    keyword = query.first.strip.downcase
    case
    when keyword == PARKING_SLOT_CREATE then [:create_slot, query.last.to_i]
    when keyword == PARKING_SLOT_PARK then [:park_car, query[1..2]]
    when keyword == PARKING_SLOT_LEAVE then [:vacate_slot, query.last.to_i]
    when keyword == PARKING_SLOT_STATUS then [:status, nil]
    when keyword == PARKING_SLOT_REG_NUM then [:reg_num_car, query.last]
    when keyword == PARKING_SLOT_NUMS then [:slot_number_cars, query.last]
    when keyword == PARKING_SLOT_NUM then [:slot_number_car, query.last]
    else [:no_match, keyword]
    end
  end 
end