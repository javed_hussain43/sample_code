require_relative "./slot"
class ParkingLot
  attr_accessor :slots
  
  def initialize(no_of_slots)
    @slots = []
    (1..no_of_slots).to_a.each do |index|
      slots[index-1] = Slot.new(index)
    end
    puts "Created​ ​a​ ​parking​ ​lot​ ​with​ ​#{no_of_slots} ​slots"
  end

  def park_car vehicle_details
    free_slot = find_free_slot
    if free_slot
      free_slot.park(vehicle_details)
      puts "Allocated slot number : #{ free_slot.id }"
    else
      puts 'Sorry, parking lot is full'
    end
  end

  def no_match input
    puts "Your input is not correct #{input}"
  end

  def vacate_slot slot_number
    if slot_number > 0 && slot_number <= slots.size
      slots[slot_number-1].free
      puts "Slot number #{slot_number} is free "
    else
      puts "Invalid slot number"
    end
  end

  def reg_num_car color
    filtered_cars = filter_cars('vehicle_number', 'vehicle_color', color)
    puts filtered_cars.compact.join(',')
  end

  def slot_number_cars color
    filtered_cars = filter_cars('id', 'vehicle_color', color)
    puts filtered_cars.compact.join(',')
  end

  def slot_number_car vehicle_number
    slot = slots.find do |slot|
      slot.vehicle_number == vehicle_number
    end
    puts slot ? slot.id : 'Not Found'
  end


  def status test
    puts "Slot No.\t Registration Number\t Colour"
    slots.each do | slot |
      puts "#{ slot.id }\t\t #{ slot.vehicle_number }\t\t #{ slot.vehicle_color }" unless (slot.free?)
    end
  end

  private

  def find_free_slot
    slots.find do |slot|
      slot.free?
    end
  end

  def filter_cars(filtered_value, filter_by, filter)
    slots.collect do |slot|
      slot.send(filtered_value) if slot.send(filter_by) == filter.downcase
    end
  end
end