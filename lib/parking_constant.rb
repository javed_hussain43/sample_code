module ParkingConstant

  PARKING_SLOT_CREATE = "create_parking_lot".freeze
  PARKING_SLOT_PARK = "park".freeze
  PARKING_SLOT_LEAVE = "leave".freeze
  PARKING_SLOT_STATUS = "status".freeze
  PARKING_SLOT_REG_NUM = "registration_numbers_for_cars_with_colour".freeze
  PARKING_SLOT_NUMS = "slot_numbers_for_cars_with_colour".freeze
  PARKING_SLOT_NUM = "slot_number_for_registration_number".freeze
  WRONG_INPUT = "Wromg keyword Entered".freeze
end